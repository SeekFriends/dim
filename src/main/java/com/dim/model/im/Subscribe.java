package com.dim.model.im;

import lombok.Data;

import java.io.Serializable;

/**
 * 事件订阅对象
 *
 * @Author: dcg
 * @Date: 2024/1/15
 */
@Data
public class Subscribe implements Serializable {

    private static final long serialVersionUID = 7330802770992819958L;

    private short command;

    private ImPacket value;

    /**
     * 生成订阅对象
     *
     * @param cmd
     * @param value
     * @return
     */
    public static Subscribe genSubscribe(short cmd, ImPacket value) {
        Subscribe subscribe = new Subscribe();
        subscribe.setCommand(cmd);
        subscribe.setValue(value);
        return subscribe;
    }

}
