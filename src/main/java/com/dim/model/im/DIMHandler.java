package com.dim.model.im;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 命令码handler注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface DIMHandler {

    short key();
}