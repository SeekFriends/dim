
package com.dim.model.im;

import io.netty.util.AttributeKey;

import java.util.Date;
import java.util.List;


/**
 * channel存储标签
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
public interface ChannelAttr {

    AttributeKey<Integer> PING_COUNT = AttributeKey.valueOf("ping_count");

    /**
     * uid
     */
    AttributeKey<String> UID = AttributeKey.valueOf("uid");

    /**
     * channel id
     */
    AttributeKey<String> ID = AttributeKey.valueOf("id");

    /**
     * 设备id
     */
    AttributeKey<Short> DEVICE_ID = AttributeKey.valueOf("device_id");

    /**
     * 原始设备id
     */
    AttributeKey<Short> DEVICE_ID_INIT = AttributeKey.valueOf("device_id_init");

    /**
     * 语言
     */
    AttributeKey<String> LANGUAGE = AttributeKey.valueOf("language");

    /**
     * 时区
     */
    AttributeKey<String> TIME_ZONE = AttributeKey.valueOf("time_zone");

    /**
     * session id
     */
    AttributeKey<String> SESSION_ID = AttributeKey.valueOf("session_id");

    /**
     * 身份检查
     */
    AttributeKey<Boolean> CHECK = AttributeKey.valueOf("check");

    /**
     * 连接时间
     */
    AttributeKey<Date> CONNECT_DATE = AttributeKey.valueOf("connect_date");

    /**
     * 订阅列表
     */
    AttributeKey<List<Subscribe>> EVENT_LIST = AttributeKey.valueOf("event_list");
}
