package com.dim.model.im;

import cn.hutool.core.util.StrUtil;
import com.dim.utils.JsonUtil;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;


/**
 * 消息体包
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@Data
public class ImPacket implements Serializable {

    private static final long serialVersionUID = -5410948102103923646L;

    /**
     * str
     */
    private String bodyStr;

    /**
     * command
     */
    private Short command;

    /**
     * data
     */
    private Object data;


    public static ImPacket toPacket(short command, Object object) {
        ImPacket imPacket = new ImPacket();
        imPacket.setCommand(command);
        if (object != null) {
            imPacket.setData(object);
        }
        return imPacket;
    }

    public byte[] toByteArray() {
        return (formatShortWithZero(command, 5) + (data != null ? JsonUtil.toJson(data) : StrUtil.EMPTY_JSON)).getBytes(StandardCharsets.UTF_8);
    }


    public static String formatShortWithZero(short num, int length) {
        return String.format("%0" + length + "d", num);
    }

}
