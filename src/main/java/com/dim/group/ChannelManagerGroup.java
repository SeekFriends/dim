package com.dim.group;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.dim.model.im.ChannelAttr;
import com.dim.model.im.ImPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * channel管理器
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@Component
public class ChannelManagerGroup extends ConcurrentHashMap<String, Collection<Channel>> {

    private static final long serialVersionUID = -9021216390902126655L;

    /**
     * 监听器
     */
    private final transient ChannelFutureListener remover = new ChannelFutureListener() {
        @Override
        public void operationComplete(ChannelFuture future) {
            future.removeListener(this);
            remove(future.channel());
        }
    };

    /**
     * 根据channel 获取uid
     *
     * @param channel
     * @return
     */
    protected String getKey(Channel channel) {
        return channel.attr(ChannelAttr.UID).get();
    }


    /**
     * channel 是否已被管理
     *
     * @param channel
     * @return
     */
    public boolean isManaged(Channel channel) {

        String uid = getKey(channel);

        if (uid == null || !channel.isActive() || Boolean.TRUE.equals(!channel.attr(ChannelAttr.CHECK).get())) {
            return false;
        }
        Collection<Channel> channels = getOrDefault(uid, Collections.emptyList());
        if (CollectionUtil.isEmpty(channels)) {
            for (Channel value : channels) {
                if (StrUtil.equals(channel.attr(ChannelAttr.ID).get(), value.attr(ChannelAttr.ID).get())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * uid是否存在
     *
     * @param uid
     * @return
     */
    public boolean exists(String uid) {
        Collection<Channel> channels = this.get(uid);
        return CollectionUtil.isNotEmpty(channels);
    }

    /**
     * 删除channel
     *
     * @param channel
     */
    public void remove(Channel channel) {
        String uid = getKey(channel);
        if (uid == null) {
            channel.close();
            return;
        }
        Collection<Channel> collections = getOrDefault(uid, Collections.emptyList());
        collections.remove(channel);
        if (CollectionUtil.isEmpty(collections)) {
            this.remove(uid);
        }
        channel.close();
    }


    /**
     * 添加channel
     *
     * @param channel
     */
    public void add(Channel channel) {
        if (!channel.isActive()) {
            channel.close();
            return;
        }
        String uid = channel.attr(ChannelAttr.UID).get();
        channel.closeFuture().addListener(remover);
        Collection<Channel> collections = this.putIfAbsent(uid, new ConcurrentLinkedQueue<>(Collections.singleton(channel)));
        if (CollectionUtil.isNotEmpty(collections)) {
            for (Channel cal : collections) {
                if (Objects.equals(cal.attr(ChannelAttr.DEVICE_ID).get(), channel.attr(ChannelAttr.DEVICE_ID).get())) {
                    collections.remove(cal);
                    cal.close();
                }
            }
            collections.add(channel);
        }
        if (!channel.isActive()) {
            remove(channel);
        }
    }

    /**
     * 向channel写入数据
     *
     * @param key
     * @param message
     */
    public void write(String key, ImPacket message) {
        Collection<Channel> channels = this.get(key);
        if (CollectionUtil.isEmpty(channels)) {
            return;
        }
        for (Channel channel : channels) {
            channel.writeAndFlush(message);
        }
    }


}