
package com.dim.utils;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONWriter;
import com.dim.write.StrDateWriter;
import com.dim.write.StrDecimalWriter;
import com.dim.write.StrSqlDateWriter;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * json序列化工具类
 * Timestamp 类型不进行序列化 通过毫秒来转换为不同时区日期
 * bigdecimal 类型为匹配app 全部序列化为string
 * 可调用put自行添加相关序列化方式
 */
@Slf4j
public class JsonUtil {

    static {
        JSON.register(Date.class, StrDateWriter.INSTANCE);
        JSON.register(java.sql.Date.class, StrSqlDateWriter.INSTANCE);
        JSON.register(BigDecimal.class, StrDecimalWriter.INSTANCE);
    }

    public static <T> T toBean(String jsonString, Class<T> tt) {
        try {
            if (StrUtil.isBlank(jsonString)) {
                return null;
            }
            return JSON.parseObject(jsonString, tt);
        } catch (Exception e) {
            log.error(jsonString, e);
            return null;
        }
    }

    public static <T> List<T> toList(String jsonString, Class<T> clazz) {
        try {
            if (StrUtil.isBlank(jsonString)) {
                return null;
            }
            return JSON.parseArray(jsonString, clazz);
        } catch (Exception e) {
            log.error(jsonString, e);
            return null;
        }
    }

    /**
     * 默认不写入null
     *
     * @param bean
     * @return
     */
    public static String toJson(Object bean) {
        return JSON.toJSONString(bean, JSONWriter.Feature.ReferenceDetection);
    }

    /**
     * 转为byte[]
     *
     * @param bean
     * @return
     */
    public static byte[] toJsonBytes(Object bean) {
        return JSON.toJSONBytes(bean, JSONWriter.Feature.ReferenceDetection);
    }

    /**
     * 写入null
     *
     * @param bean
     * @return
     */
    public static String toJsonAboutNull(Object bean) {
        return JSON.toJSONString(bean, JSONWriter.Feature.WriteNulls, JSONWriter.Feature.ReferenceDetection);
    }

    /**
     * json convert obj
     *
     * @param jsonStr
     * @return
     */
    public static JSONObject toJsonObj(String jsonStr) {
        return JSON.parseObject(jsonStr);
    }

}
