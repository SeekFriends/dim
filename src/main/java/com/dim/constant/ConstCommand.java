package com.dim.constant;

/**
 * 相当于command
 *
 * @Author: dcg
 * @Date: 2023/10/27
 */
public interface ConstCommand {

    /**
     * 心跳请求
     */
    short HeartbeatReq = (short) 1;

    /**
     * 心跳响应
     */
    short HeartbeatResp = (short) 2;


    /**
     * 错误
     */
    short ErrorResp = (short) 500;

    /**
     * 自定义握手连接
     */
    short HandshakeConnection = (short) 599;


    /**
     * 最终关闭前业务处理
     */
    short CloseBeforeChannel = (short) 3;


    /**
     * 最终关闭channel
     */
    short CloseAfterChannel = (short) 4;

    /**
     * socket字节长度
     */
    int DATA_HEADER_LENGTH = 5;

    /**
     * socket压缩标准
     */
    int BODY_SIZE_COMPRESS = 300;
}
