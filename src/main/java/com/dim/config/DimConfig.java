package com.dim.config;

import com.dim.config.properties.socket.DIMSocketProperties;
import com.dim.config.properties.websocket.DIMWebsocketProperties;
import com.dim.constant.ConstCommand;
import com.dim.handler.DIMRequestHandler;
import com.dim.model.im.DIMHandler;
import com.dim.model.im.ImPacket;
import com.dim.server.AppSocketAcceptorServer;
import com.dim.server.WebsocketAcceptorServer;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * dim配置类
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@Configuration
@EnableConfigurationProperties({DIMWebsocketProperties.class, DIMSocketProperties.class})
public class DimConfig implements DIMRequestHandler, ApplicationListener<ApplicationStartedEvent> {

    @Autowired
    private ApplicationContext applicationContext;


    private final HashMap<Short, DIMRequestHandler> handlerMap = new HashMap<>();


    @Bean(destroyMethod = "destroy", initMethod = "bind")
    @ConditionalOnProperty(name = {"dim.websocket.enable"}, matchIfMissing = true)
    public WebsocketAcceptorServer websocketAcceptor(DIMWebsocketProperties dimWebsocketProperties) {
        dimWebsocketProperties.setOuterRequestHandler(this);
        return new WebsocketAcceptorServer(dimWebsocketProperties);
    }

    @Bean(destroyMethod = "destroy", initMethod = "bind")
    @ConditionalOnProperty(name = {"dim.socket.enable"}, matchIfMissing = true)
    public AppSocketAcceptorServer appSocketAcceptor(DIMSocketProperties dimSocketProperties) {
        dimSocketProperties.setOuterRequestHandler(this);
        return new AppSocketAcceptorServer(dimSocketProperties);
    }


    @Override
    public void process(Channel channel, ImPacket imPacket) {

        short command = imPacket.getCommand();
        DIMRequestHandler handler = handlerMap.get(command);

        if (handler == null) {
            if (Objects.equals(command, ConstCommand.CloseBeforeChannel)) {
                handler = handlerMap.get(ConstCommand.CloseAfterChannel);
            } else {
                return;
            }
            handler.process(channel, imPacket);
            return;
        }
        if (!Objects.equals(command, ConstCommand.CloseBeforeChannel)) {
            handler.process(channel, imPacket);
            return;
        }
        handler.process(channel, imPacket);
        handler = handlerMap.get(ConstCommand.CloseAfterChannel);
        handler.process(channel, imPacket);
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {

        Map<String, DIMRequestHandler> beans = applicationContext.getBeansOfType(DIMRequestHandler.class);

        for (Map.Entry<String, DIMRequestHandler> entry : beans.entrySet()) {

            DIMRequestHandler handler = entry.getValue();

            DIMHandler annotation = handler.getClass().getAnnotation(DIMHandler.class);

            if (annotation != null) {
                handlerMap.put(annotation.key(), handler);
            }
        }
    }
}
