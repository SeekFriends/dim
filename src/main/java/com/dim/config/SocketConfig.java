package com.dim.config;


import com.dim.handler.DIMRequestHandler;
import lombok.Data;

import java.time.Duration;

/**
 * SocketConfig
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@Data
public class SocketConfig {

    private boolean useSsl;

    private boolean enable;

    private Integer port;

    private String path;

    /**
     * 服务端未向到客户端写入信息超时时间
     */
    private Duration writeIdle = Duration.ofMillis(45000);

    /**
     * 服务端未接收到客户端发送信息超时时间
     */
    private Duration readIdle = Duration.ofMillis(60000);

    /**
     * 超时次数
     */
    private int maxPongTimeout = 1;

    private DIMRequestHandler outerRequestHandler;

    public void setWriteIdle(Duration writeIdle) {
        if (writeIdle == null || writeIdle.toMillis() < 0) {
            return;
        }
        this.writeIdle = writeIdle;
    }

    public void setReadIdle(Duration readIdle) {
        if (readIdle == null || readIdle.toMillis() < 0) {
            return;
        }
        this.readIdle = readIdle;
    }


    public void setMaxPongTimeout(int maxPongTimeout) {
        if (maxPongTimeout <= 0) {
            return;
        }
        this.maxPongTimeout = maxPongTimeout;
    }

    public boolean isUseSsl() {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }
}
