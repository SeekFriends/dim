package com.dim.config.properties.websocket;


import com.dim.config.SocketConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * websocket
 * 配置properties
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ConfigurationProperties(prefix = "dim.websocket")
public class DIMWebsocketProperties extends SocketConfig {

    private static final int DEFAULT_PORT = 23456;

    private Integer port;

    /**
     * 编解码模式
     */
    private String codeMode;

    @Override
    public Integer getPort() {
        return port == null || port <= 0 ? DEFAULT_PORT : port;
    }


}
