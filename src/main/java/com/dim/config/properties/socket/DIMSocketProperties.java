package com.dim.config.properties.socket;


import com.dim.config.SocketConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * socket
 * 配置properties
 *
 * @Author: dcg
 * @Date: 2023/11/2
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ConfigurationProperties(prefix = "dim.socket")
public class DIMSocketProperties extends SocketConfig {

    private static final int DEFAULT_PORT = 19998;

    private Integer port;

    @Override
    public Integer getPort() {
        return port == null || port <= 0 ? DEFAULT_PORT : port;
    }

}
