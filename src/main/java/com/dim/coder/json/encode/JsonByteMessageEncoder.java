package com.dim.coder.json.encode;

import cn.hutool.core.util.ZipUtil;
import com.dim.constant.ConstCommand;
import com.dim.model.im.ImPacket;
import com.dim.utils.JsonUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * json byte 编码器
 *
 * @Author: dcg
 * @Date: 2023/11/2
 */
public class JsonByteMessageEncoder extends MessageToByteEncoder<ImPacket> {

    private static final Logger logger = LoggerFactory.getLogger(JsonByteMessageEncoder.class);

    /**
     * 消息体：2字节length + 2字节command + 1字节gzip + 数据
     * 扩容消息体：2字节-10000 + 4字节length + 2字节command + 1字节gzip + 数据
     *
     * @param ctx
     * @param data
     * @param out
     */
    @Override
    protected void encode(final ChannelHandlerContext ctx, final ImPacket data, ByteBuf out) {
        try {
            short command = data.getCommand();
            byte[] bodys = null;
            if (data.getData() != null) {
                bodys = JsonUtil.toJson(data.getData()).getBytes(StandardCharsets.UTF_8);
            }
            int gzip = 0;
            if (bodys != null) {
                // gzip
                if (bodys.length > ConstCommand.BODY_SIZE_COMPRESS) {
                    try {
                        bodys = ZipUtil.gzip(bodys);
                    } catch (Exception e) {
                        logger.error("socket编码压缩时错误！", e);
                        return;
                    }
                    gzip = 1;
                }
                // 长度
                if (bodys.length > Short.MAX_VALUE) {
                    out.writeShort(-10000);
                    out.writeInt(bodys.length);
                } else {
                    out.writeShort(bodys.length);
                }
                out.writeShort(command);
                out.writeByte(gzip);
                out.writeBytes(bodys);
            } else { // 写入空数据
                out.writeShort(0);
                out.writeShort(command);
                out.writeByte(gzip);
            }
        } catch (Exception e) {
            logger.error("socket 编码 异常 数据体！", e);
        }
    }
}
