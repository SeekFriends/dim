package com.dim.coder.json.encode;

import com.dim.model.im.ImPacket;
import com.dim.utils.JsonUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * json 字符串 编码
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
public class JsonStrMessageEncoder extends MessageToMessageEncoder<ImPacket> {

    private static final Logger logger = LoggerFactory.getLogger(JsonStrMessageEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, ImPacket imPacket, List<Object> list) throws Exception {
        try {
            if (imPacket != null) {
                TextWebSocketFrame frame = new TextWebSocketFrame(JsonUtil.toJson(imPacket));
                list.add(frame);
            }
        } catch (Exception e) {
            logger.error("websocket 编码 异常 数据体！", e);
        }
    }
}
