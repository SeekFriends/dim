package com.dim.coder.json.encode;

import com.dim.model.im.ImPacket;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * json Binary 编码
 *
 * @Author: dcg
 * @Date: 2024/1/9
 */
public class JsonBinaryMessageEncoder extends MessageToMessageEncoder<ImPacket> {

    private static final Logger logger = LoggerFactory.getLogger(JsonBinaryMessageEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, ImPacket imPacket, List<Object> list) throws Exception {
        try {
            if (imPacket != null) {
                ByteBuf buffer = Unpooled.wrappedBuffer(imPacket.toByteArray());
                BinaryWebSocketFrame frame = new BinaryWebSocketFrame(buffer);
                list.add(frame);
            }
        } catch (Exception e) {
            logger.error("websocket 编码 异常 数据体！", e);
        }

    }
}
