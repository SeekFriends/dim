package com.dim.coder.json.decode;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.dim.constant.ConstCommand;
import com.dim.model.im.ChannelAttr;
import com.dim.model.im.ImPacket;
import com.dim.utils.JsonUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Objects;

/**
 * json 字符串 解码
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
public class JsonStrMessageDecoder extends MessageToMessageDecoder<TextWebSocketFrame> {

    private static final Logger logger = LoggerFactory.getLogger(JsonStrMessageDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext context, TextWebSocketFrame msg, List<Object> list) throws Exception {
        String text = msg.text();
        if (StrUtil.isBlank(text)) {
            return;
        }
        short command = 0;
        try {
            JSONObject parseObject = JsonUtil.toJsonObj(text);
            command = parseObject.getShort("command");
            if (!Objects.equals(ConstCommand.HeartbeatResp, command)) {
                Object data = parseObject.get("data");
                ImPacket imPacket = new ImPacket();
                imPacket.setBodyStr(JsonUtil.toJson(data));
                imPacket.setCommand(command);
                list.add(imPacket);
            }
            context.channel().attr(ChannelAttr.PING_COUNT).set(null);
        } catch (Exception e) {
            logger.error("websocket 解码 异常 数据体！", e);
            InetSocketAddress socketAddress = (InetSocketAddress) context.channel().remoteAddress();
            logger.error("Command：{} , 来源：{} , 原始数据：{} ", command, socketAddress.getAddress().getHostAddress(), text);
        }
    }
}
