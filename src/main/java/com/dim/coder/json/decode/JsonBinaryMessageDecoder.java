package com.dim.coder.json.decode;

import cn.hutool.core.util.StrUtil;
import com.dim.constant.ConstCommand;
import com.dim.model.im.ChannelAttr;
import com.dim.model.im.ImPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * json 二进制 解码器
 *
 * @Author: dcg
 * @Date: 2024/1/9
 */
public class JsonBinaryMessageDecoder extends MessageToMessageDecoder<BinaryWebSocketFrame> {

    private static final Logger logger = LoggerFactory.getLogger(JsonBinaryMessageDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, BinaryWebSocketFrame msg, List<Object> queue) throws Exception {
        short command = 0;
        try {
            ByteBuf buffer = msg.content();
            if (buffer.readableBytes() < ConstCommand.DATA_HEADER_LENGTH) {
                return;
            }
            byte[] commandBytes = new byte[ConstCommand.DATA_HEADER_LENGTH];
            buffer.readBytes(commandBytes);
            command = Short.parseShort(new String(commandBytes, StandardCharsets.UTF_8));

            if (Objects.equals(ConstCommand.HeartbeatResp, command)) {
                ctx.channel().attr(ChannelAttr.PING_COUNT).set(null);
                return;
            }
            int byteLength = buffer.readableBytes();
            ImPacket imPacket = new ImPacket();
            imPacket.setCommand(command);
            if (byteLength <= 0) {
                imPacket.setBodyStr(StrUtil.EMPTY_JSON);
            } else {
                byte[] jsonBytes = new byte[byteLength];
                buffer.readBytes(jsonBytes);
                String bodyStr = new String(jsonBytes, StandardCharsets.UTF_8);
                imPacket.setBodyStr(bodyStr);
            }
            queue.add(imPacket);
            ctx.channel().attr(ChannelAttr.PING_COUNT).set(null);
        } catch (Exception e) {
            logger.error("websocket 解码 异常 数据体！", e);
            InetSocketAddress socketAddress = (InetSocketAddress) ctx.channel().remoteAddress();
            logger.error("Command：{} , 来源：{} ", command, socketAddress.getAddress().getHostAddress());
        }
    }
}
