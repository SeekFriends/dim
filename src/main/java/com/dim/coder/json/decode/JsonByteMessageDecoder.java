package com.dim.coder.json.decode;

import cn.hutool.core.util.ZipUtil;
import com.dim.constant.ConstCommand;
import com.dim.model.im.ChannelAttr;
import com.dim.model.im.ImPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;


/**
 * json byte 解码器
 *
 * @Author: dcg
 * @Date: 2023/11/2
 */
public class JsonByteMessageDecoder extends ByteToMessageDecoder {

    private static final Logger logger = LoggerFactory.getLogger(JsonByteMessageDecoder.class);
    //private static final ConcurrentHashMap<String, AtomicInteger> CURRENT_HASH_MAP = new ConcurrentHashMap<>();

    /**
     * 解码器
     * 消息体：2字节length + 2字节command + 1字节gzip + 数据
     * 扩容消息体：2字节-10000 + 4字节length + 2字节command + 1字节gzip + 数据
     *
     * @throws Exception
     */
    @Override
    protected void decode(ChannelHandlerContext context, ByteBuf buffer, List<Object> queue) throws Exception {
        short command = 0;
        try {
            // 消息体不足5位，发生断包情况
            if (buffer.readableBytes() < ConstCommand.DATA_HEADER_LENGTH) {
                return;
            }
            buffer.markReaderIndex();
            // 长度
            int realLength = 0;
            short length = buffer.readShort();
            if (length < 0) {
                realLength = ConstCommand.DATA_HEADER_LENGTH + 2;
                if (buffer.readableBytes() < realLength) {
                    buffer.resetReaderIndex();
                    return;
                }
                realLength += buffer.readInt();
            } else {
                realLength = length;
            }
            // command
            command = buffer.readShort();
            // gizp
            byte gzip = buffer.readByte();

            // 发生断包情况，等待接收完成
            if (buffer.readableBytes() < realLength) {
                buffer.resetReaderIndex();
                return;
            }
            /* 读完 心跳刷新 */
            if (!Objects.equals(ConstCommand.HeartbeatResp, command)) {
                boolean isGzipped = Objects.equals(gzip, (byte) 1);
                byte[] content = new byte[length];
                buffer.readBytes(content);
                if (isGzipped) {
                    content = ZipUtil.unGzip(content);
                }
                String s = new String(content, StandardCharsets.UTF_8);
                // 封装成packet
                ImPacket imPacket = new ImPacket();
                imPacket.setCommand(command);
                imPacket.setBodyStr(s);
                queue.add(imPacket);
            }
            context.channel().attr(ChannelAttr.PING_COUNT).set(null);
        } catch (Exception e) {
            // TODO: 计数
            logger.error("socket 解码 异常 数据体！", e);
            InetSocketAddress socketAddress = (InetSocketAddress) context.channel().remoteAddress();
            logger.error("Command：{} , 来源：{} ", command, socketAddress.getAddress().getHostAddress());
        }
    }

}
