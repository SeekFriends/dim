package com.dim.handler;

import com.dim.model.im.Subscribe;
import io.netty.channel.Channel;

import java.util.List;

/**
 * 事件订阅处理
 *
 * @Author: dcg
 * @Date: 2024/1/15
 */
public interface EventSubProcessRouteFactory {

    /**
     * 处理订阅事件
     *
     * @param channel
     * @param eventList
     */
    void processEvent(Channel channel, List<Subscribe> eventList);
}
