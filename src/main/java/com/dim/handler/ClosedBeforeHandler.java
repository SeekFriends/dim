package com.dim.handler;

/**
 * channel关闭前业务处理
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
public abstract class ClosedBeforeHandler implements DIMRequestHandler {
}
