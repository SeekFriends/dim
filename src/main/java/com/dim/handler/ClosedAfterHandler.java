
package com.dim.handler;


import cn.hutool.core.util.StrUtil;
import com.dim.constant.ConstCommand;
import com.dim.group.ChannelManagerGroup;
import com.dim.model.im.ChannelAttr;
import com.dim.model.im.DIMHandler;
import com.dim.model.im.ImPacket;
import io.netty.channel.Channel;
import lombok.AllArgsConstructor;

/**
 * 关闭channel处理
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@DIMHandler(key = ConstCommand.CloseAfterChannel)
@AllArgsConstructor
public class ClosedAfterHandler implements DIMRequestHandler {

    private final ChannelManagerGroup channelManagerGroup;

    @Override
    public void process(Channel channel, ImPacket message) {
        String uid = channel.attr(ChannelAttr.UID).get();
        if (StrUtil.isNotBlank(uid)) {
            channelManagerGroup.remove(channel);
            return;
        }
        channel.close();
    }

}
