package com.dim.handler.handshake;


import io.netty.channel.Channel;


/**
 * websocket握手处理接口
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
public interface HandshakeRequestHandler {

   void check(HandshakeEvent event, Channel channel);
}
