package com.dim.handler;


import com.dim.model.im.ImPacket;
import io.netty.channel.Channel;


/**
 * 命令码处理接口
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
public interface DIMRequestHandler {

	void process(Channel channel, ImPacket packet);
}
