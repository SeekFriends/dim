package com.dim.write;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.writer.ObjectWriter;

import java.lang.reflect.Type;
import java.sql.Date;
import java.time.format.DateTimeFormatter;

/**
 * json2 date
 *
 * @Author: dcg
 * @Date: 2024/1/9
 */
public class StrSqlDateWriter implements ObjectWriter<Date> {

    public static final StrSqlDateWriter INSTANCE = new StrSqlDateWriter();

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public void write(JSONWriter jsonWriter, Object object, Object fieldName, Type fieldType, long features) {
        if (object == null) {
            jsonWriter.writeNull();
            return;
        }
        Date date = (Date) object;
        String format = DateUtil.format(date, formatter);
        jsonWriter.writeString(format);
    }

    private StrSqlDateWriter() {

    }
}




