package com.dim.write;

import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.writer.ObjectWriter;

import java.lang.reflect.Type;
import java.math.BigDecimal;

/**
 * json2 decimal
 *
 * @Author: dcg
 * @Date: 2024/1/9
 */
public class StrDecimalWriter implements ObjectWriter<BigDecimal> {

    public static final StrDecimalWriter INSTANCE = new StrDecimalWriter();

    @Override
    public void write(JSONWriter jsonWriter, Object object, Object fieldName, Type fieldType, long features) {
        if (object == null) {
            jsonWriter.writeNull();
            return;
        }
        BigDecimal decimal = (BigDecimal) object;
        String format;
        if (decimal.scale() > 0) {
            format = decimal.stripTrailingZeros().toPlainString();
        } else {
            format = decimal.toPlainString();
        }
        jsonWriter.writeString(format);
    }

    private StrDecimalWriter() {

    }
}
