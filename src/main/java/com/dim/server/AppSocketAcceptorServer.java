package com.dim.server;

import com.dim.coder.json.decode.JsonByteMessageDecoder;
import com.dim.coder.json.encode.JsonByteMessageEncoder;
import com.dim.config.properties.socket.DIMSocketProperties;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * socket启动配置
 *
 * @Author: dcg
 * @Date: 2023/11/2
 */
@Slf4j
@ChannelHandler.Sharable
public class AppSocketAcceptorServer extends NioSocketAcceptorServer {

    public AppSocketAcceptorServer(DIMSocketProperties config) {
        super(config);
    }


    @Override
    public void bind() {
        if (!socketConfig.isEnable()) {
            return;
        }
        ServerBootstrap bootstrap = createServerBootstrap();
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) {
                //if (socketConfig.isUseSsl()) {
                //    SslContext sslContext = createSslContext();
                //    ch.pipeline().addLast(sslContext.newHandler(ch.alloc()));
                //}
                ch.pipeline().addLast(new JsonByteMessageDecoder());
                ch.pipeline().addLast(new JsonByteMessageEncoder());
                ch.pipeline().addLast(loggingHandler);
                ch.pipeline().addLast(new IdleStateHandler(socketConfig.getReadIdle().toMillis(), socketConfig.getWriteIdle().toMillis(), 0, TimeUnit.MILLISECONDS));
                ch.pipeline().addLast(AppSocketAcceptorServer.this);
            }
        });

        ChannelFuture channelFuture = bootstrap.bind(socketConfig.getPort()).syncUninterruptibly();
        channelFuture.channel().newSucceededFuture().addListener(future -> {
            log.info("Socket Server started for [JSON byte] mode. port: {}", socketConfig.getPort());
            log.info("writeTimeOut: {} , readTimeOut: {} , maxPongTimeout: {} ", socketConfig.getWriteIdle().toMillis(), socketConfig.getReadIdle().toMillis(), socketConfig.getMaxPongTimeout());
        });
        // 异步关闭
        channelFuture.channel().closeFuture().addListener(future -> this.destroy());
    }


    //private SslContext createSslContext() {
    //    FileInputStream fileInputStream = null;
    //    try {
    //        // 加载.jks格式的SSL证书
    //        KeyStore keyStore = KeyStore.getInstance("JKS");
    //        fileInputStream = new FileInputStream(KEY_STORE_PATH);
    //        keyStore.load(fileInputStream, KEY_STORE_PASSWORD.toCharArray());
    //
    //        // 创建KeyManagerFactory和TrustManagerFactory
    //        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    //        keyManagerFactory.init(keyStore, KEY_STORE_PASSWORD.toCharArray());
    //        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    //        trustManagerFactory.init(keyStore);
    //
    //        // 创建SSL上下文
    //        return SslContextBuilder.forServer(keyManagerFactory)
    //                .trustManager(InsecureTrustManagerFactory.INSTANCE)
    //                .build();
    //    } catch (Exception e) {
    //        logger.error("使用ssl失败！", e);
    //        System.exit(1);
    //    } finally {
    //        try {
    //            if (fileInputStream != null) {
    //                fileInputStream.close();
    //            }
    //        } catch (IOException e) {
    //            logger.error("", e);
    //        }
    //    }
    //    return null;
    //}
}
