package com.dim.server;

import cn.hutool.core.util.StrUtil;
import com.dim.coder.json.decode.JsonBinaryMessageDecoder;
import com.dim.coder.json.decode.JsonStrMessageDecoder;
import com.dim.coder.json.encode.JsonBinaryMessageEncoder;
import com.dim.coder.json.encode.JsonStrMessageEncoder;
import com.dim.config.properties.websocket.DIMWebsocketProperties;
import com.dim.handler.handshake.HandshakeHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;


/**
 * websocket启动配置
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@Slf4j
@ChannelHandler.Sharable
public class WebsocketAcceptorServer extends NioSocketAcceptorServer {

    private final HandshakeHandler handshakeHandler;

    public WebsocketAcceptorServer(DIMWebsocketProperties config) {
        super(config);
        this.handshakeHandler = new HandshakeHandler();
    }

    @Override
    public void bind() {
        DIMWebsocketProperties config = (DIMWebsocketProperties) socketConfig;
        if (!socketConfig.isEnable()) {
            return;
        }
        ServerBootstrap bootstrap = createServerBootstrap();
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

            @Override
            public void initChannel(SocketChannel ch) {
                ch.pipeline().addLast(new HttpServerCodec());
                ch.pipeline().addLast(new ChunkedWriteHandler());
                ch.pipeline().addLast(new HttpObjectAggregator(4 * 1024));
                ch.pipeline().addLast(new WebSocketServerProtocolHandler(socketConfig.getPath(), true));
                ch.pipeline().addLast(handshakeHandler);
                if (StrUtil.equalsIgnoreCase(config.getCodeMode(), "str")) {
                    ch.pipeline().addLast(new JsonStrMessageDecoder());
                    ch.pipeline().addLast(new JsonStrMessageEncoder());
                } else {
                    ch.pipeline().addLast(new JsonBinaryMessageDecoder());
                    ch.pipeline().addLast(new JsonBinaryMessageEncoder());
                }
                ch.pipeline().addLast(new IdleStateHandler(socketConfig.getReadIdle().toMillis(), socketConfig.getWriteIdle().toMillis(), 0, TimeUnit.MILLISECONDS));
                ch.pipeline().addLast(loggingHandler);
                ch.pipeline().addLast(WebsocketAcceptorServer.this);
            }

        });
        ChannelFuture channelFuture = bootstrap.bind(socketConfig.getPort()).syncUninterruptibly();
        channelFuture.channel().newSucceededFuture().addListener(future -> {
            log.info("DIM Websocket Server started for [JSON {}] mode. port: {}  path: {} ", config.getCodeMode(), socketConfig.getPort(), socketConfig.getPath());
            log.info("DIM writeTimeOut: {} , readTimeOut: {} , maxPongTimeout: {} ", socketConfig.getWriteIdle().toMillis(), socketConfig.getReadIdle().toMillis(), socketConfig.getMaxPongTimeout());
        });
        channelFuture.channel().closeFuture().addListener(future -> this.destroy());
    }

}
