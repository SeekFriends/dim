package com.dim.server;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.dim.config.SocketConfig;
import com.dim.constant.ConstCommand;
import com.dim.group.ChannelManagerGroup;
import com.dim.handler.EventSubProcessRouteFactory;
import com.dim.model.im.ChannelAttr;
import com.dim.model.im.ImPacket;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import oshi.SystemInfo;
import oshi.software.os.OperatingSystem;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ThreadFactory;

/**
 * channel事件处理
 *
 * @Author: dcg
 * @Date: 2023/10/26
 */
@Slf4j
abstract class NioSocketAcceptorServer extends SimpleChannelInboundHandler<ImPacket> {

    protected final ChannelHandler loggingHandler = new LoggingHandler();

    public final SocketConfig socketConfig;

    private final EventLoopGroup bossGroup;
    private final EventLoopGroup workerGroup;

    private static final EventSubProcessRouteFactory EVENT_SUB_PROCESS_ROUTE_FACTORY = SpringUtil.getBean(EventSubProcessRouteFactory.class);
    private static final ChannelManagerGroup CHANNEL_MANAGER_GROUP = SpringUtil.getBean("channelManagerGroup", ChannelManagerGroup.class);
    private static final OperatingSystem operatingSystem = new SystemInfo().getOperatingSystem();

    protected NioSocketAcceptorServer(SocketConfig socketConfig) {

        this.socketConfig = socketConfig;

        ThreadFactory bossThreadFactory = r -> {
            Thread thread = new Thread(r);
            thread.setName("nio-boss-" + thread.getId());
            return thread;
        };

        ThreadFactory workerThreadFactory = r -> {
            Thread thread = new Thread(r);
            thread.setName("nio-worker-" + thread.getId());
            return thread;
        };

        if (isLinuxSystem()) {
            bossGroup = new EpollEventLoopGroup(bossThreadFactory);
            workerGroup = new EpollEventLoopGroup(workerThreadFactory);
        } else {
            bossGroup = new NioEventLoopGroup(bossThreadFactory);
            workerGroup = new NioEventLoopGroup(workerThreadFactory);
        }
    }


    public abstract void bind();


    public void destroy() {
        if (bossGroup != null && !bossGroup.isShuttingDown() && !bossGroup.isShutdown()) {
            try {
                bossGroup.shutdownGracefully();
            } catch (Exception ignore) {
            }
        }

        if (workerGroup != null && !workerGroup.isShuttingDown() && !workerGroup.isShutdown()) {
            try {
                workerGroup.shutdownGracefully();
            } catch (Exception ignore) {
            }
        }
    }

    protected ServerBootstrap createServerBootstrap() {
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup);
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
        bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.channel(isLinuxSystem() ? EpollServerSocketChannel.class : NioServerSocketChannel.class);
        return bootstrap;
    }

    /**
     * 通道活跃
     *
     * @param ctx
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        if (ctx.channel().attr(ChannelAttr.ID).get() == null) {
            ctx.channel().attr(ChannelAttr.CONNECT_DATE).set(new Date());
            ctx.channel().attr(ChannelAttr.ID).set(ctx.channel().id().asShortText());
            ctx.channel().attr(ChannelAttr.EVENT_LIST).set(new ArrayList<>(3));
            Boolean check = ctx.channel().attr(ChannelAttr.CHECK).get();
            if (check == null || !check) {
                ctx.channel().attr(ChannelAttr.CHECK).set(false);
            }
        }
    }

    /**
     * 读取信息
     *
     * @param ctx
     * @param data
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ImPacket data) {
        try {
            if (!Objects.equals(ConstCommand.HandshakeConnection, data.getCommand())) {
                if (Boolean.FALSE.equals(ctx.channel().attr(ChannelAttr.CHECK).get())) {
                    log.error("未认证的连接:{},ip:{}", ctx.channel(), ctx.channel().remoteAddress());
                    CHANNEL_MANAGER_GROUP.remove(ctx.channel());
                    return;
                }
            }
        } catch (Exception e) {
            log.error("异常Command 数据：{}", data.toString());
            log.error("", e);
            return;
        }

        if (socketConfig.getOuterRequestHandler() != null) {
            socketConfig.getOuterRequestHandler().process(ctx.channel(), data);
        }
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) {

        if (ctx.channel().attr(ChannelAttr.UID) == null) {
            ctx.channel().close();
            return;
        }
        if (socketConfig.getOuterRequestHandler() == null) {
            ctx.channel().close();
            return;
        }

        ImPacket imPacket = ImPacket.toPacket(ConstCommand.CloseBeforeChannel, null);
        socketConfig.getOuterRequestHandler().process(ctx.channel(), imPacket);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {

        if (!(evt instanceof IdleStateEvent)) {
            return;
        }

        IdleStateEvent idleEvent = (IdleStateEvent) evt;

        Channel channel = ctx.channel();

        boolean readFlag = idleEvent.state() == IdleState.READER_IDLE;
        Integer pingCount = channel.attr(ChannelAttr.PING_COUNT).get();
        if (readFlag && pingCount != null && pingCount >= socketConfig.getMaxPongTimeout()) {
            log.info("{} 心跳超时 断开连接.{}", channel.attr(ChannelAttr.UID).get(), channel.remoteAddress());
            ImPacket imPacket = ImPacket.toPacket(ConstCommand.CloseBeforeChannel, null);
            socketConfig.getOuterRequestHandler().process(ctx.channel(), imPacket);
            return;
        } else if (readFlag && (pingCount == null || pingCount < socketConfig.getMaxPongTimeout())) {
            channel.attr(ChannelAttr.PING_COUNT).set(pingCount == null ? 1 : ++pingCount);
            ImPacket imPacket = ImPacket.toPacket(ConstCommand.HeartbeatReq, null);
            channel.writeAndFlush(imPacket);
            return;
        }
        if (!readFlag && (idleEvent.state() == IdleState.WRITER_IDLE)) {
            EVENT_SUB_PROCESS_ROUTE_FACTORY.processEvent(channel, channel.attr(ChannelAttr.EVENT_LIST).get());
            return;
        }
        // 终止异常的channel
        CHANNEL_MANAGER_GROUP.remove(ctx.channel());
    }

    private boolean isLinuxSystem() {
        return StrUtil.equalsIgnoreCase("linux", operatingSystem.getFamily());
    }

}
