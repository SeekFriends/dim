# DIM

#### 介绍
基于远方夕阳大佬的CIM开源项目。

(CIM地址：https://gitee.com/farsunset/cim?_from=gitee_search)

进行二次修改 IM架子

适配自己业务所需所以进行了修改

注重于业务的开发

自定义自己业务所需命令码

下面是已实现的命令码 只需实现追加即可

![输入图片说明](image.png)

实现注解，进行业务开发即可。


修改了websocket，和socket 并使用了fastjson2

事例：

dim:

  websocket:

    path: /market

    enable: true

    write-idle: 0

    read-idle: 15000

    max-pong-timeout: 2

    port: 13369


后面有时间在进行文档代码的完善和修改






